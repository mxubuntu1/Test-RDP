echo Create User
sudo useradd -m MxGenius && sudo adduser MxGenius sudo && echo 'MxGenius:MxGenius' | sudo chpasswd

echo Apt Update
sudo apt update

echo Install: Ubuntu Desktop
sudo apt install ubuntu-desktop -y

echo Download: Chrome
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb

echo Install: Chrome
sudo dpkg --install google-chrome-stable_current_amd64.deb
sudo apt install --assume-yes --fix-broken

echo Install: Xbase Clients
sudo apt-get install xbase-clients python3-psutil xvfb -y

echo Install: Chrome Remote Desktop
wget https://dl.google.com/linux/direct/chrome-remote-desktop_current_amd64.deb
sudo dpkg --install chrome-remote-desktop_current_amd64.deb
sudo apt install --assume-yes --fix-broken
